/* 
 * Dinamic behavior of other text field
 */
(function($) {
  Drupal.behaviors.taxonomyOrOtherShowOther = {
    attach: function (context, settings) {
      $('.field-type-taxonomy-or-other .form-select', context).change(function() {
        $this = $(this);
        var $formWapper = $this.parents('.field-type-taxonomy-or-other');
        if ($this.val() === '_other') {
          $('.form-type-textfield', $formWapper).show();
        } else {
          $('.form-type-textfield', $formWapper).hide();
        }
      }).change();
    }
  };
})(jQuery);

